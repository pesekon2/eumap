Package: eumap
Title: European environmental maps processing and production
Version: 0.0.2
Authors@R: 
   c(person("Tomislav", "Hengl", email = "tom.hengl@opengeohub.org", role = c("aut", "cre")),
     person("Mohammadreza", "Sheykhmousa", email = "mohammadreza.sheykhmousa@opengeohub.org", role = c("aut")),
     person("Leandro", "Leal Parente", email = "leandro.parente@opengeohub.org", role = c("aut")),
     person("Carmelo", "Bonannella", email = "carmelo.bonannella@opengeohub.org", role = c("aut")))
Description: Functions and sample datasets for accessing, processing and producing spatial layers for EU. It includes standard tiling systems, functions to overlay points and large stacks of grids, fit spatial prediction and harmonization models, harmonize multisource Earth Observation Data. Find out more about this project from: https://opendatascience.eu/.
Depends: R (>= 3.5.0)
License: GPL-2 | MIT + file LICENSE
Encoding: UTF-8
LazyData: true
URL: https://gitlab.com/geoharmonizer_inea/eumap
BugReports: https://gitlab.com/geoharmonizer_inea/eumap/issues/
Imports: methods, utils, ranger, nnet, glmnet, mlr3
Suggests: rgdal, parallel, terra, raster, xgboost, matrixStats
RoxygenNote: 7.1.1
SystemRequirements: C++11, GDAL (>= 2.0.1), GEOS (>= 3.4.0), PROJ (>=
        4.8.0)
NeedsCompilation: no
Packaged: 2020-11-27 09:31:41 UTC; tomislav
Author: Tomislav Hengl [aut, cre],
  Mohammadreza Sheykhmousa [aut],
  Leandro Leal Parente [aut],
  Carmelo Bonannella [aut]
Maintainer: Tomislav Hengl <tom.hengl@opengeohub.org>
