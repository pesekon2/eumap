import requests
import tarfile
import tempfile
import os, sys
import threading
import time
import re
from functools import reduce
from operator import add
from typing import Union, List
import shutil

DATASETS = [
    '4582_spain_landcover_samples.gpkg',
    '4582_spain_rasters.tar.gz',
    '4582_spain_rasters_gapfilled.tar.gz',
    '5606_greece_landcover_samples.gpkg',
    '5606_greece_rasters.tar.gz',
    '5606_greece_rasters_gapfilled.tar.gz',
    '9326_italy_landcover_samples.gpkg',
    '9326_italy_rasters.tar.gz',
    '9326_italy_rasters_gapfilled.tar.gz',
    '9529_croatia_landcover_samples.gpkg',
    '9529_croatia_rasters.tar.gz',
    '9529_croatia_rasters_gapfilled.tar.gz',
    '10636_switzerland_landcover_samples.gpkg',
    '10636_switzerland_rasters.tar.gz',
    '10636_switzerland_rasters_gapfilled.tar.gz',
    '14576_netherlands_landcover_samples.gpkg',
    '14576_netherlands_rasters.tar.gz',
    '14576_netherlands_rasters_gapfilled.tar.gz',
    '14580_netherlands_landcover_samples.gpkg',
    '14580_netherlands_rasters.tar.gz',
    '14580_netherlands_rasters_gapfilled.tar.gz',
    '15560_poland_landcover_samples.gpkg',
    '15560_poland_rasters.tar.gz',
    '15560_poland_rasters_gapfilled.tar.gz',
    '16057_ireland_landcover_samples.gpkg',
    '16057_ireland_rasters.tar.gz',
    '16057_ireland_rasters_gapfilled.tar.gz',
    '22497_sweden_landcover_samples.gpkg',
    '22497_sweden_rasters.tar.gz',
    '22497_sweden_rasters_gapfilled.tar.gz',
    'eu_tilling system_30km.gpkg',
]
ALL = DATASETS

KEYWORDS = reduce(add, map(
    lambda ds_name: re.split(r'[\s_\d\.]+', ds_name),
    DATASETS
))
KEYWORDS = sorted(set(filter(
    lambda kw: kw not in ('', 'km', 'eu', 'system'),
    KEYWORDS
)))

TILES = sorted(set(reduce(add, map(
    lambda ds_name: re.findall(r'\d+_[a-z]+', ds_name),
    DATASETS
))))

DATA_ROOT_NAME = 'eumap_data'
_CHUNK_LENGTH = 2**13
_DOWNLOAD_DIR = os.getcwd()
_PROGRESS_INTERVAL = .2 # seconds

def get_datasets(keywords: Union[str, List[str]]='') -> list:
    if isinstance(keywords, str):
        keywords = [keywords]
    return [*filter(
        lambda ds_name: sum([
            keyword in ds_name
            for keyword in keywords
        ]) == len(keywords),
        ALL
    )]

def _make_download_request(dataset:str) -> requests.Response:
    url = f'https://zenodo.org/record/4265220/files/{dataset}?download=1'
    return requests.get(url, stream=True)

class _DownloadWorker:

    def __init__(self, dataset:str, download_dir: str=_DOWNLOAD_DIR):
        self.done = False
        self.error = None
        self.dataset = dataset
        self.download_dir = download_dir
        self.progress = 0
        self.downloaded = 0
        self.size = None
        self.tmp_filepath = os.path.join(download_dir, DATA_ROOT_NAME, f'tmp_{dataset}.part')
        self._stopped = False

    def _unpack(self):
        datapath = os.path.join(self.download_dir, DATA_ROOT_NAME)
        try:
            tile_name = next(filter(
                lambda tile: self.dataset.startswith(tile),
                TILES
            ))
            datapath = os.path.join(datapath, tile_name)
        except StopIteration:
            pass

        if not os.path.isdir(datapath):
            os.makedirs(datapath)

        if self.dataset.endswith('tar.gz'):
            with tarfile.open(self.tmp_filepath, "r:gz") as archive:
                archive.extractall(datapath)
            os.remove(self.tmp_filepath)
        else:
            shutil.move(
                self.tmp_filepath,
                os.path.join(datapath, self.dataset)
            )

    def _download(self):
        try:
            with _make_download_request(self.dataset) as resp:
                resp.raise_for_status()
                self.size = int(resp.headers.get('content-length'))
                with open(self.tmp_filepath, 'wb') as dst:
                    for chunk in resp.iter_content(_CHUNK_LENGTH):
                        if self._stopped:
                            return
                        dst.write(chunk)
                        dst.flush()
                        self.downloaded += _CHUNK_LENGTH
                        self.progress = (100 * self.downloaded) // self.size
            self._unpack()
            self.done = True
        except Exception as e:
            self.error = e

    def _clean(self):
        try:
            os.remove(self.tmp_filepath)
        except FileNotFoundError:
            pass

    def start(self):
        self.thread = threading.Thread(target=self._download)
        self.thread.start()

    def stop(self):
        if not self.done:
            self._stopped = True
            self.thread.join()
            self._clean()

def get_data(datasets: Union[str, list], download_dir: str=_DOWNLOAD_DIR):
    try:
        os.makedirs(os.path.join(download_dir, DATA_ROOT_NAME))
    except FileExistsError:
        pass

    if datasets == 'all':
        datasets = DATASETS
    if isinstance(datasets, str):
        datasets = [datasets]
    for dataset in datasets:
        if dataset not in DATASETS:
            print(f'Dataset {dataset} not available, please choose "all" or one/more of the following:')
            for ds_name in DATASETS:
                print('\t • '+ds_name)
            return

    workers = [
        _DownloadWorker(ds, download_dir)
        for ds in datasets
    ]
    for w in workers:
        w.start()
    n_downloads = len(workers)
    n_failed = 0
    sizes = [None for w in workers]
    total_size = None

    try:
        while True:
            time.sleep(_PROGRESS_INTERVAL)

            workers_alive = []
            for w in workers:
                if w.error is None:
                    workers_alive.append(w)
                else:
                    print(f'Error downloading {w.dataset}: {w.error}'+' '*20)
                    n_failed += 1
                    w.stop()
            workers = workers_alive

            if None in sizes:
                sizes = [w.size for w in workers]
                not_started = sum((s is None for s in sizes))
                print(f'Starting {not_started} downloads...', end='\r')
                continue
            else:
                total_size = sum(sizes)

            n_done = sum((w.done for w in workers))
            all_done = n_done == len(workers)
            total_download = sum((w.downloaded for w in workers))
            non_failed_total_size = sum((w.size for w in workers))
            total_progress = (100 * total_download) // non_failed_total_size
            if total_progress < 100:
                print(
                    f'{total_progress}% of {n_downloads} downloads / ' \
                    f'{round(total_download/2**20, 2)} of {round(total_size/2**20, 2)} MB ' \
                    f'({n_failed} failed, {n_done} done)'+' '*20,
                    end='\r',
                )
            else:
                print(
                    f'{round(total_download/2**20, 2)} of {round(total_download/2**20, 2)} ' \
                    f'MB downloaded ({n_failed} failed), unpacking...' + ' '*20, end='\r'
                )
            if all_done:
                print('\nDownload complete.')
                break

    except KeyboardInterrupt as e:
        for w in workers:
            w.stop()
        print('\nInterrupted')

if __name__ == '__main__':
    get_data(DATASETS)
